import os
import uuid
from flask import *
from flaskext.couchdb import CouchDBManager
import requests
import datetime


COUCHDB_SERVER = 'http://194.29.175.241:5984'
COUCHDB_DATABASE = 'chat'
SECRET_KEY = 'development key'

app = Flask(__name__)
manager = CouchDBManager()
manager.setup(app)
app.config.from_object(__name__)

@app.route('/register')
def registerApp():
    try:
        dataToRegisterApplication = {'host' : 'http://limitless-island-4957.herokuapp.com', 'active' : True, 'delivery' : '/add' }
        g.couch['limitless-island-4957'] = dataToRegisterApplication
        g.couch.save(dataToRegisterApplication)
        return 'Zarejestrowano aplikacje w couch DB pod kluczem: limitless-island-4957'
    except:
        return 'Ta aplikacja zostala juz zarejestrowana i jest aktywna!'

@app.route('/index')
def index():
    result = getAllMessagesFromFile()
    tabWithMessages = []
    for x in result:
        tabWithMessages.append(x)
    return render_template('index.html', messages=tabWithMessages)

@app.route('/add', methods=['GET', 'POST'])
def add():
    messageValue = ''
    if request.method == 'POST':
        messageValue = request.form['message']
        try:
            adddMessageToFile(session['loggedUser'] + ':     ' + messageValue)
        except:
            pass
    try:
        tabWithLinks = getActiveLinks()
        sendedMessage = json.dump({"user": session['loggedUser'], "message":messageValue, "time":str(datetime.datetime.time), "id":str(uuid.uuid4())})
        for link in tabWithLinks:
            sessionObj = requests.session()
            sessionObj.post(link, data=sendedMessage)
    except:
        print ('nieudane wyslanie')
    result = getAllMessagesFromFile()
    tabWithMessages = []
    for x in result:
        tabWithMessages.append(x)
    return render_template('index.html', messages=tabWithMessages)

@app.route('/login', methods=['GET', 'POST'])
def Login():
    if request.method == 'POST':
        doLogin(request.form['loginValue'])
    else:
        return render_template('loginForm.html')
    result = getAllMessagesFromFile()
    tabWithMessages = []
    for x in result:
        tabWithMessages.append(x)
    return render_template('index.html', messages=tabWithMessages)



@app.route('/logout')
def logOut():
    session.pop('login', None)
    session.pop('loggedUser', None)
    result = getAllMessagesFromFile()
    tabWithMessages = []
    for x in result:
        tabWithMessages.append(x)
    return render_template('index.html', messages=tabWithMessages)


def getAllMessagesFromFile():
    lines = ''
    fileToOpen = open(os.path.join('static', 'messages.txt'), "r")
    lines = fileToOpen.readlines()
    fileToOpen.close()
    return lines

def adddMessageToFile(parameter):
    lines = ''
    fileToOpen = open(os.path.join('static', 'messages.txt'), "r")
    lines = fileToOpen.readlines()
    fileToOpen.close()
    fileToOpen = open(os.path.join('static', 'messages.txt'), "w")
    for x in lines:
        fileToOpen.write(x)
    fileToOpen.write(parameter)
    fileToOpen.write('\n')
    fileToOpen.close()

def doLogin(userLogin):
    if userLogin != None:
        session['login'] = True
        session['loggedUser'] = userLogin
        print('zalogowany')
    else:
        pass

def getActiveLinks():
    listWithLinks = []
    url = "http://194.29.175.241:5984/chat/_design/utils/_view/list_active"
    session = requests.session()
    odp = session.get(url)
    slownik = odp.json()
    i = 0
    for x in slownik["rows"]:
        listWithLinks.append(slownik["rows"][i]['value']['host'] + slownik["rows"][i]['value']['delivery'])
        i = i+1
    return listWithLinks



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=int(os.getenv('PORT', 5000)), debug=True)
    #app.run(debug=True)